<link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."main.css")?>">

<link rel = "stylesheet" type = "text/css" href = "<?=auto_version(FRONT_CSS."material.css")?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel = "stylesheet" href = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel = "shortcut icon" href = "<?=auto_version(FRONT_ASSETS."img/american-favicon.png")?>" />
<link rel="stylesheet" href="https://use.typekit.net/fdq2bno.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">




<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=auto_version(FRONT_JS."main.js")?>"></script>

<script src="<?=auto_version(FRONT_JS."material.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."nouislider.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."js.cookie.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."isotope.min.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."velocity.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."wNumb.js")?>"></script>
<!-- <script src="<?=auto_version(FRONT_JS."timeit.js")?>"></script> -->
<script src="<?=auto_version(FRONT_LIBS."/iscroll/iscroll.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."jquery.gdocsviewer.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."jquery.simplesidebar.js")?>"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">


<script src="<?=auto_version(FRONT_LIBS."jquery.zoom.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."gifshot/gifshot.js")?>"></script>
<script type="text/javascript" src="<?=auto_version(FRONT_LIBS."slick/slick/slick.min.js")?>"></script>
<link rel="stylesheet" type="text/css" href="<?=auto_version(FRONT_LIBS."slick/slick/slick.css")?>"/>
<link rel="stylesheet" type="text/css" href="<?=auto_version(FRONT_LIBS."slick/slick/slick-theme.css")?>"/>

<link rel="stylesheet" href="<?=auto_version(FRONT_LIBS."scrollbar/jquery.mCustomScrollbar.css")?>" />
<script src="<?=auto_version(FRONT_LIBS."scrollbar/jquery.mCustomScrollbar.concat.min.js")?>"></script>

<script src="<?=auto_version(FRONT_LIBS."slidesjs/jquery.slides.min.js")?>"></script>
<script src="https://momentjs.com/downloads/moment.min.js" ></script>


