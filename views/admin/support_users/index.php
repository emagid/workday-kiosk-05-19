<?php
if(count($model->support_users) > 0) { ?>
	<div class="box box-table">
		<table class="table">
			<thead>
				<tr>
					<th width=2%>Id</th>
					<th width=10%>User's Phone No.</th>
					<th width=10%>Support Contacted</th>
					<!-- <th width="5%" class="text-center">Edit</th> -->
          			<th width="5%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model->support_users as $obj){  
					$support = \Model\Support::getItem($obj->support_list_id); ?>
					<tr>
						<td><?php echo $obj->id;?></a></td>
						<td><?php echo $obj->user_phone;?></a></td>
						<td><?php echo $support->support_list;?></a></td>				
				        <td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>support_users/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
				           		<i class="icon-cancel-circled"></i> 
				           	</a>
				        </td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
<? } else { ?>
	<h4>None users contacted support yet. </h4>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'support_users';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>