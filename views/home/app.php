<div class='content'>
  <div class='apppage'>
	  <section class='screens'>
	  	<div class='screen' style='background-color: #004e7f'>
	  		<h1>One app for everything you need in a workday</h1>
	  		<div class='qr'>
		  		<img src="<?= FRONT_ASSETS ?>img/qr.png">
		  		<p>Open your phone camera and scan the qr to donwload the app.</p>
	  		</div>
	  	</div>
	  </section>
	  <div class='iphone'>
	  	<p class='script'>Touch below for a demo!</p>
	  	<img class='phone_img' src="<?= FRONT_ASSETS ?>img/iphone.png">
	  	<div class='route route_home'></div>
	  	<div class='routes'>
	  		<div data-id='route1' class='route1 route'></div>
<!-- 	  		<div data-id='route2' class='route2 route'></div>
	  		<div data-id='route3' class='route3 route'></div>
	  		<div data-id='route4' class='route4 route'></div> -->
	  	</div>

	  	<div class='step_one step active'>
	  		<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
	  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic1.png">
	  	</div>

  		<div class='app_screen' id='route1'>
  			<div class='step_two step active'>
  				<img class='hand' src="<?= FRONT_ASSETS ?>img/hand.png">
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic2.png">
		  	</div>
		  	<div class='step_five step'>
		  		<img class='phone' src="<?= FRONT_ASSETS ?>img/pic3.png">
		  	</div>
  		</div>
	  </div>
  </div>

  <script type="text/javascript">
	var touchTimer;

	function isWhite(s){
		if ( $(s).hasClass('white') ) { 
			$('.iphone .script').addClass('black');
		 }else {
		 	$('.iphone .script').removeClass('black');
		 }
	}

	$(document).on('click', '.route', function(){
		var id = '#'+$(this).attr('data-id');
		var div = $($(id).children('.step')[0]);

		nextStep();
		$(id).fadeIn(300);
		div.fadeIn(300)
	});	

	$(document).on('click', '.route_home', function(){
		reset();
	});	

	function nextStep(){
		$('.routes').hide();
		$('.step_one').fadeOut(300);
	}	

	$(document).on('click', '.step', function(){
		if ( $(this).hasClass('step_five') ) {
			reset();
		}else {
			$(this).fadeOut(300)
			$(this).next('div').fadeIn(300).addClass('active');
			$(this).removeClass('active');
		}
	});

	function reset(s){
		$('.routes').show();
		$('.step_one').fadeIn(300);
		$('.app_screen').fadeOut(300);
		$('.app_screen .step').removeClass('active').hide();
		setTimeout(function(){
			$('.app_screen').each(function(){
				$($(this).children('.step')[0]).addClass('active').show();
			});
		}, 300);
	}

    function invoke() {
        touchTimer = window.setTimeout(
            function() {
                reset($($('.active')[0]));
            }, 30000);
    }

    invoke();

    $('body').on('click mousemove', function(){
        window.clearTimeout(touchTimer);
        invoke();
    });
  </script>

</div>