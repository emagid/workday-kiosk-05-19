<div class='content'>

  <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
  <div class='contentpage franchisepage' style=
  "background-image: url('<?= FRONT_ASSETS ?>img/franchise.png');">
  	<h1>Interested in Franchising with Matto?</h1>
  	<p>Enter your email below, and we'll send you a form to fill out so we can talk about all the benefits of franchising with Matto.</p>
  	<form id='form'>
  		<input id='email' type="email" name="email" placeholder='Type your email here'>
  		<input type="submit" value='Submit'>
  	</form>
  </div>
<section id='share_alert' style="background-image: url('<?= FRONT_ASSETS ?>img/thankyou.jpg');">

  <script type="text/javascript">
  	$(document).on('submit', '#form', function(e){
  		e.preventDefault();
		data = {
			email: $('#email').val()
		}
		$('#share_alert').fadeIn(500);
        $.post('/contact/franchise',data,function(data){
            if(data.status) {
            	setTimeout(function(){
            		window.location = '/';
            	}, 5000);
            } else {
            	setTimeout(function(){
            		window.location = '/';
            	}, 5000);
            }
        });
        setTimeout(function(){
    		window.location = '/';
    	}, 8000);
    });
  </script>

</div>