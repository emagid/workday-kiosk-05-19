<div class='content'>
    <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">
    <div class='home_wheel'>        
    <img class='home' src="<?= FRONT_ASSETS ?>img/home.png">
    <section class='main resting'>
        <div class='prize'>
            <div>
                <img src="">
                <p>YOUR PRIZE <span></span></p>
            </div>
        </div>

        <div class='pre_spin'>
            <div class='intro'>
                <p>ENTER EMAIL</p>
                <p>TO <span>WIN</span></p>
            </div>
            <input id="prize_email" class='jQKeyboard email_wheel' type="text" name="email" placeholder='Email'>
            <button class='button'>SPIN THE WHEEL!</button>
            <p class='error'>Please enter a valid email!</p>
        </div>

        <div class='wheel'>
          <img class='spin' src="<?= FRONT_ASSETS ?>img/wheel.png">
          <img class='ticker' src="<?= FRONT_ASSETS ?>img/ticker.png">
        </div>
    </section>
    </div>

    
    <script src="<?=auto_version(FRONT_JS."keyboard.js")?>"></script>
    <script type="text/javascript">
       $(document).on('click', '.pre_spin .button', function(){
        if (checkValid()) {
            $('.error').fadeOut();
            $('.main').css('pointer-events', 'none');
            $('.pre_spin').fadeOut(500);
            $('.jQKeyboardContainer').hide();
            $('.main').removeClass('resting');
            $('.spin').css('transform', 'rotate(0deg)');
            $('.spin').addClass('ready');
            $.post('/contact/save_contact/',{email: $('#prize_email').val(), form: 4 }, (response) => {
                if(response.status){
                    console.log('success');
                } else {
                    console.log('failure');
                }
            })
            setTimeout(function(){
                startSpin();
            }, 1000);
        }else {
            console.log('here')
            $('.error').fadeIn();
            }
       });

       function checkValid() {
            var val = $('.email_wheel').val();
            var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

            if ( re.test(val) ) {
                return true;
            }else {
                return false;
            }
       }

       // if ( prize.chance ) { num = worsenOdds(prize, num) }
       //  $('.ready').css('transform', 'rotate('+ num +'deg)');

       rotation = 0

       function startSpin() {
        rotation = randNum();
        console.log(rdm)
        $('.main').css('pointer-events', 'none');
        var winNum = -(rotation % 360) + 360;
        findPrize(winNum);
       }

       function randNum() {
        rdm = Math.floor(Math.random() * Math.floor(360));
        var num =  rdm + 360*9;
        if ( num % 10 === 0 ) {
            num + 4;
        }rotation
        return num
       }

       function findPrize(num) {
        prizes.forEach(function(prize, i){
            if ( num >= prize.min && prize.min +60 >= num ) {
                checkOdds(prize, num);
            }
        });
       }

       function checkOdds(prize, num) {
        console.log('odds')
        if ( prize.chance ) { 
            worsenOdds(prize, num) 
        }else {
            showPrize(prize, num);
        }
       }

       function worsenOdds( prize, num ) {
        var randomNum = Math.floor(Math.random() * Math.floor(prize.chanceNum));
        if ( randomNum == 0 ) {
        }else {
            num = Math.floor(Math.random() * Math.floor(360));
        }
        findPrize(num)
       }

       var prizes = [
            {
                name: 'Water Bottle',
                imgUrl: '<?= FRONT_ASSETS ?>img/water.png',
                min: 0,
                color: '#42d1d1',
                chance: true,
                chanceNum: 4
            },
            {
                name: 'Phone Case',
                imgUrl: '<?= FRONT_ASSETS ?>img/phonecase.png',
                min: 60,
                color: '#60e8ef'
            },
            {
                name: 'Hand Sanitizer',
                imgUrl: '<?= FRONT_ASSETS ?>img/sanitizer.png',
                min: 120,
                color: '#42d1d1'
            },
            {
                name: 'Chalp Balm',
                imgUrl: '<?= FRONT_ASSETS ?>img/chapbalm.png',
                min: 180,
                color: '#60e8ef'
            },
            {
                name: 'Phone Case',
                imgUrl: '<?= FRONT_ASSETS ?>img/phonecase.png',
                min: 240,
                color: '#60e8ef'
            },
            {
                name: 'Hand Sanitizer',
                imgUrl: '<?= FRONT_ASSETS ?>img/sanitizer.png',
                min: 300,
                color: '#42d1d1'
           } 
        ]

        function showPrize(prize, num) {
            console.log(((360-num) + (360*9)));
            $('.ready').css('transform', 'rotate('+ ((360-num) + (360*9)) +'deg)');
            setTimeout(function(){
                $('.prize').fadeIn(1000);
                $('.prize img').attr('src', prize.imgUrl);
                $('.prize span').html(prize.name + '!');
                $('.prize').css('background-color', prize.color);

                setTimeout(function(){
                    window.location = '/';
                }, 5000)
            }, 7500);
        }

        var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});
                $('textarea.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.number_key').initKeypad({'donateKeyboardLayout': board});
            });
    </script>
</div>