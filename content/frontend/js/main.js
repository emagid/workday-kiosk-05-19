$(document).ready(function(){


    // DISABLE RIGHT CLICKS
    document.addEventListener('contextmenu', event => event.preventDefault());


    // CLICK ACTION
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.8)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });


    // ROUTING
    $(document).on('click', '.click', function(){        
        var self = this;
        $(self).css('transform', 'scale(.95)');
        setTimeout(function(){
            $(self).css('transform', 'scale(1)');
            activeCover();
        }, 200);
        changePage($(self).attr('data-page'));
    });

    $(document).on('click', '.home', function(){
        window.location = '/';
    });

    $(document).on('click', '.back', function(){
        changePage('projects');
    });

    function changePage(page) {
        $('.cover').css('background-color', '#29abca');
        $.ajax({
            url:'/home/'+page,
                type:'GET',
                success: function(data){
                var timer;
                  $('.jQKeyboardContainer').hide();
                    timer = setTimeout(function(){
                        activeCover();
                        $('.content').fadeOut(500);
                        timer = setTimeout(function(){
                            $('.content').html($(data).find('.content').html()).fadeIn();                            
                        }, 2000);
                    }, 100);
                }
        });
    }

    $(document).on('click', '.link', function(){
        kioskPage(this);
    });

    function kioskPage(el) {
        timer;
        page = $(el).attr('id');
        
        $.ajax({
            url:'/home/'+page,
            type:'GET',
            success: function(data){
              $('.jQKeyboardContainer').hide();
                timer = setTimeout(function(){
                    activeCover();
                    $('.content').fadeOut(500);
                    timer = setTimeout(function(){
                        $('.content').html($(data).find('.content').html()).fadeIn();                  
                    }, 2000);
                }, 100);
            }
        });
    }

    function activeCover() {
        $('.cover').addClass('open');
        setTimeout(function(){
            $('.cover').removeClass('open');
        }, 2000);
    }



    // TIMEOUT
    var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    activeCover();
                    setTimeout(function(){
                        window.location.href = '/home/wheel';
                    }, 1500);
                }, 24000000);
        }


        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
            // invoke2();
        })



});



