<?php

use Twilio\Rest\Client;

class contactController extends siteController
{

    public function franchise_post(){
        $mergeTags = $_POST; 
        $email = new \Email\MailMaster();
        $email->setTo(['email' => $_POST['email'], 'type' => 'to'])
              ->noBCC()
              ->setMergeTags($mergeTags)
              ->setSubject('Thank You!')->setTemplate('matto-franchise');
        try {
            $emailresp = $email->send();
            $this->toJson(['status'=>true]);
        } catch (Mandrill_Error $e) {
            $this->toJson(['status'=>false,"message"=>"Failed to send franchise email"]);
        }
    }

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | Popshap";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $response = ['status'=>false,
                     'msg'=>'failed to Send'];

        $obj = \Model\Contact::loadFromPost();
        // $email = new \Model\Email;

        if(isset($_POST['email'])){
            $email = implode(",", $_POST['email']);
        } else {
            $email = "";
        }

        if(isset($_POST['phone'])){
            $phone = implode(",", $_POST['phone']);
        } else {
            $phone = "";
        }

        if(count($_POST['email']) > 0){
            // $obj->email = $_POST['email'][0];
            $obj->email = trim($email, ',');
        } else {
            $obj->email = '';
        }
        if(count($_POST['phone']) > 0){
            // $obj->phone = $_POST['phone'][0];
            $obj->phone = trim($phone, ',');
        } else {
            $obj->phone = '';
        }
       
        if($obj->save()){

            // for($i=0; $i<count($_POST['email']); $i++) {
            //     $email->email = $_POST['email'][$i];
            //     $email->contact_id = $obj->id;
            //     $email->save();
            // }
            

            $imgs = [];
            $gif = [];
            $email_image = null;
            if(isset($_POST['image'])){
                $img = $_POST['image'];
                $image = \Model\Snapshot_Contact::getItem($img);
                $image->contact_id = $obj->id;
                if($image->save()){
                    $response['image']=$image;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$image->get_image_url();
                }
            } else if(isset($_POST['gif'])){
                $_gif = $_POST['gif'];
                $gif = \Model\Gif::getItem($_gif);
                $gif->contact_id = $obj->id;
                $gif->image = str_replace('\\','/',$gif->image);
                if($gif->save()){
                    $response['gif'] = $gif;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$gif->get_image_url();
                }
            }

            $email = new \Email\MailMaster();
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);

            $email_responses = [];
            $phone_responses = [];

            foreach ($_POST['email'] as $_email){
                if($_email == '') continue;
                if($email_image == null)
                    $eimage = '<p>Thanks for attending!</p>';
                else
                    $eimage = $email_image;
                $mergeTags = [
                    'CONTENT'=>"<img style='max-width: 728px; width: 100%;'src='$eimage'>"
                ];
                $email_responses[$_email] = $email->setTo(['email' => $_email, 'name' => ucwords($obj->name), 'type' => 'to'])->setSubject('Thank You!')->setTemplate('matto-photobooth')->setMergeTags($mergeTags)->send();
            }
            foreach ($_POST['phone'] as $phone){
                if($phone == '') continue;
                $phone_responses[$phone] = $client->messages
                    ->create(
                        $phone,
                        array(
                            "from" => TWILIO_NUMBER,
                            "body" => "Here's your picture!! ". $email_image,
                        )
                    );
            }

            $response['phones'] = $phone_responses;
            $response['emails'] = $email_responses;
            $response['status'] = true;
            $response['msg'] = 'Success';
            $response['contact'] = $obj;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function make_gif_post(){
        $response = ['status'=>true];
        if(isset($_POST['images'])){
            $imgs = [];
            foreach($_POST['images'] as $img){

                $image = $this->save_img($img);
                $imgs[] = $image;
            }
            $response['images'] = $imgs;
            
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function make_frame_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');
        if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image'] = $image;
        } else {
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function save_img_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');

        if(isset($_POST['gif'])){
            $gif = new \Model\Gif();
            $gif->frames = json_encode($_POST['images']);
            foreach($_POST['images'] as $img){
                \Model\Snapshot_Contact::delete($img);
            }

            $img = $_POST['gif'];
            $img = str_replace('data:image/gif;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $fileName = uniqid().'.gif';
            $file = UPLOAD_PATH.'Gifs'.DS.$fileName;

            if (!is_dir(UPLOAD_PATH.'Gifs'.DS)){
                mkdir(UPLOAD_PATH.'Gifs'.DS, 0777, true);
            }

            $success = file_put_contents($file, $data);


            $gif->image = $fileName;
            $gif->contact_id = 0;
            $gif->frames = json_encode($_POST['images']);
            if($gif->save()){
                $response['gif'] = $gif;
            } else {
                $response['status'] = false;
                $response['errors'] = $gif->errors;
                $response['msg'] = "Gif Save Failed";
            }
        } else if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image']=$image;
        }
        echo json_encode($response);
    }

    public function save_img($_img, $obj = null){
        $img = $_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = uniqid().'.png';
        $file = UPLOAD_PATH.'Snapshots'.DS.$fileName;

        if (!is_dir(UPLOAD_PATH.'Snapshots'.DS)){
            mkdir(UPLOAD_PATH.'Snapshots'.DS, 0777, true);
        }

        $success = file_put_contents($file, $data);

        $image = new \Model\Snapshot_Contact();
        if($obj) $image->contact_id = $obj->id;
        else $image->contact_id = 0;
        $image->image = $fileName;
        $image->save();

        if($obj){
            $obj->save();
        }
        return($image);
    }

    public function save_contact_post(){
        $contact = \Model\Contact::loadFromPost();
        $response = ['status'=>false,'msg'=>'failure'];

        $_email = $contact->email;
        
        if($contact->save()){

            try {
                
                $email = new \Email\MailMaster();
                $mergeTags = [];
                $response['email'] = $email->setTo(['email' => $_email, 'name' => $_email, 'type' => 'to'])
                                            ->setTemplate('popshap-wheel')
                                            ->setMergeTags($mergeTags)
                                            ->send();
                                            
                $response['status'] = true;
                $response['msg'] = 'Saved';
            } catch (MadrillError $e){
                $response['msg'] = $e->getMessage();
            }

        }
        $this->toJson($response);
    }

    public function exportContacts(){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=contact_list.csv');
        
        $sql="SELECT * FROM contact WHERE active=1";

        $users = \Model\Contact::getList(['sql'=>$sql]);
        $output = fopen('php://output', 'w');
        $t=array("No.",'Email',"Phone","Image link");
        fputcsv($output, $t);
        $row ='';
         
        foreach($users as $key=>$user) {
            $image = \Model\Snapshot_Contact::getItem(null,['where'=>"contact_id = ".$user->id]);
            $image_link = 'https://'.$_SERVER['SERVER_NAME'].UPLOAD_URL.'Snapshots/'. $image->image;
            $row = array($key+1,$user->email,$user->phone,$image_link);
            fputcsv($output, $row);  
        }
    }
}