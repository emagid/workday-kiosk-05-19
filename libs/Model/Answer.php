<?php
/**
 * Created by PhpStorm.
 * User: Garrett
 * Date: 9/19/17
 * Time: 4:22 PM
 */

namespace Model;

class Answer extends \Emagid\Core\Model {
    public static $tablename = "answer";

    public static $fields = [
        'text',
        'question_id',
        // 'featured_image',
        'display_order' => ['type'=>'number']
    ];
}