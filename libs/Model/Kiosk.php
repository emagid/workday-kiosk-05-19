<?php

namespace Model;

class Kiosk extends \Emagid\Core\Model {
    static $tablename = "kiosk";

    public static $fields  =  [
    	'name'=>['required'=>true],
    	'description',
    	'slug',
		'home_page',
		'content'
    ];
}
























