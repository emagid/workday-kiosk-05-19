<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/2/15
 * Time: 11:55 AM
 */

namespace Model;


class Product_Type{
    public static $type = ['Product', 'WeddingBand', 'Ring', 'Jewelry'];
    public static $lcType = ['product', 'weddingBand', 'ring', 'jewelry'];

    public static function getTypeId($type)
    {
        $type = ucfirst($type);

        if(($key = array_search($type, self::$type)) !== false){
            return $key + 1;
        }

        throw new \Exception("Product type: {$type} is not exist");
    }

    public static function getType($key)
    {
        $key -= 1;
        if(isset(self::$type[$key])){
            return self::$type[$key];
        }

        throw new \Exception("key: {$key} is not exist");
    }
}